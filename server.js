const express = require("express");

const app = express();
let router = require("./router");
let apiRouter = require("./routes/api");

const session = require("express-session");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");

const env = app.get("env");
let secret = "vEvaRC2HEnYwFB5FybCUF9Zxmg42z6G7Bw2W";
if (env === "production") {
  const redis = require("redis");
  let RedisStore = require("connect-redis")(session);
  let redisClient = redis.createClient();
  app.use(
    session({
      store: new RedisStore({ client: redisClient }),
      secret: secret,
      saveUninitialized: true,
      resave: false
    })
  );
} else {
  app.use(
    session({
      secret: "vEvaRC2HEnYwFB5FybCUF9Zxmg42z6G7Bw2W",
      resave: true,
      saveUninitialized: true,
      cookie: { secure: "auto" }
    })
  );
}

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.set("views", "bin/views");
app.set("view engine", "ejs");
app.use("/", router);
app.use("/api", apiRouter);

app.use(cookieParser());

app.use(express.static(__dirname + "/public"));
app.use("/upload", express.static(__dirname + "/upload"));

app.listen(8000);
