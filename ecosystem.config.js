module.exports = {
    apps : [{
        script: 'server.js',
        watch: ["router.js","routes","models"],
        ignore_watch : ["node_modules", "public/images"],
        watch_options: {
            "followSymlinks": false
        },
        watch_delay: 1000,
        env: {
            NODE_ENV: "development",
        },
        env_production: {
            NODE_ENV: "production",
        }
    }],

    deploy : {
        production : {
            user : 'SSH_USERNAME',
            host : 'SSH_HOSTMACHINE',
            ref  : 'origin/master',
            repo : 'GIT_REPOSITORY',
            path : 'DESTINATION_PATH',
            'pre-deploy-local': '',
            'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
            'pre-setup': ''
        }
    }
};
