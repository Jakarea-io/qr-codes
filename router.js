const fs=require('fs');
const path=require('path');
const express = require("express");
const qrcode = require('qrcode');
const { v4: uuidv4 } = require('uuid');

const utility = require("./models/utility");
const QRcodes = require('./controllers/QrController');
const db_connections = require('./models/db_connection');

let router = express.Router();

router.get('/',(req,res)=>{
    let session=req.session;
    let messages=utility.getLang();
   
    res.render('index',{
        messages,
        session
    });
})

// router.get("/:page", async (req, res)=> {
   
//     let page=req.params.page;
//     if(page){
//         let session=req.session;
//         let messages=utility.getLang();
//         let qr = await qrcode.toDataURL(req.headers.host+'/'+uuidv4());
       
//         if(fs.existsSync(path.join(__dirname,"bin/views/"+page+".ejs"))){
//             res.render(page,{
//                 messages,
//                 qr,
//                 session
//             });
//         }else {
//             res.status(404);
//             res.render('404',{
//                 messages,
//                 session
//             });
//         }
//     }
// });

router.get("/qr", QRcodes.index);

router.post("/qr", QRcodes.create);
router.get("/qr/:uuid", QRcodes.show);
router.get("/qr/:uuid/visit", QRcodes.visit); //Only for visitors URL may be change

function checkuserloginornot(req, res, next) {
    const user_id = req.session.userid;
    if (user_id) {
        next();
    } else {
        return res.redirect("/login");
    }
}

module.exports = router;
