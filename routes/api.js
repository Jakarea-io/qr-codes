const express = require("express");
const path = require('path');
const fs = require('fs');
const bparser = require("body-parser");
const utility = require("../models/utility");
const multiparty = require('multiparty');
let router = express.Router();

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
})
const upload = multer({ storage: storage }).any()
// Problem in this line, If comment out this line, can get json data in body
//router.use(upload); 
// When this line is active, form data cant get except file, body is empty




router.use(
    bparser.json({
        limit: "50mb",
    })
);
router.use(
    bparser.urlencoded({
        extended: true,
        limit: "50mb",
    })
);

router.all("/:method/:action", (req, res) => {

    // if (req.params.action === "create-profile") {
    //     // upload(req, {}, function (err) {
    //     //     if (err) throw err
    //     //     if (req.file) {
    //     //         console.log('File Uploaded,', req.file)
    //     //     }
    //     // })

    //     var form = new multiparty.Form();

    //     form.parse(req, function(err, fields, files) {
    //       console.log(fields.first_name[0],fields.last_name[0], files.profile_pic[0] )
    //     });
    // }

    let session = req.session;
    let access = true;
    let method = req.params.method;
    let action = req.params.action;
    let token = req.headers.authorization;
    let query = req.query
    let body = {
        payload: req.body,
        action: action,
        token: token,
        query,
        files: null
    };

    if (req.params.action === "create-profile") {
        
        let form = new multiparty.Form();

        //Please check this chunk of code
        form.parse(req, (err, fields, files) => {
            let oldPath = files.profile_pic[0].path
            let newPath = path.join(__dirname, './../public/uploads')+'/'+ Date.now() + '-'+files.profile_pic[0].originalFilename

            let rawData = fs.readFileSync(oldPath)
            console.log( files.profile_pic[0].originalFilename )
            fs.writeFile(newPath, rawData, function(err){ 
                if(err) console.log(err) 
            }) 
        });



    }
    if (access) {
        if (method === "user") {
            let user = require("../models/user");
            let u = new user(action, body);
            u.output()
                .then((reply) => {
                    console.log(reply);
                    res.send(reply);
                })
                .catch((err) => {
                    res.status(500);
                    res.send(err);
                });
        } else if (method === "qr") {
            let qrCodes = require("../models/qrCodes");
            let u = new qrCodes(action, body);
            u.output()
                .then((reply) => {
                    console.log(reply);
                    res.send(reply);
                })
                .catch((err) => {
                    res.status(500);
                    res.send(err);
                });
        } else if (method === "app-user") {
            let app_user = require("../models/app_user");
            let u = new app_user(action, body);
            u.output()
                .then((reply) => {
                    console.log(reply);
                    res.send(reply);
                })
                .catch((err) => {
                    res.status(500);
                    res.send(err);
                });
        }

        else if (method === "device") {
            let device = require("../models/device");
            let u = new device(action, body);
            u.output()
                .then((reply) => {
                    console.log(reply);
                    res.send(reply);
                })
                .catch((err) => {
                    res.status(500);
                    res.send(err);
                });
        }
        else {
            res.status(404);
            res.send(utility.Response(method, "warning", "Method not found"));
        }
    }
});

module.exports = router;