const { v4: uuidv4 } = require('uuid');
const connection = require('../models/db_connection')
const utility = require("../models/utility");
const qrcode = require('qrcode');

module.exports = {

    async index(req, res) {

        let status = req.session.status;
        req.session.status = '';
        let message = req.session.message;
        req.session.message = '';
        let messages = utility.getLang();

        var result = [];
        var generateQRCode = function (callback) {
            // let path = req.protocol + '://' + req.get('host') + req.baseUrl
            // let current_page = Number(req.query.page ? req.query.page < 1 ? 1 : req.query.page : 1)
            // let per_page = 10
            // let from = current_page > 1 ? (current_page - 1) * per_page + 1 : 1

            connection.query(`SELECT * FROM qr_codes ORDER BY id DESC `, async function (err, res, fields) {
                if (err) return callback(err);
                if (res.length) {
                    for (var i = 0; i < res.length; i++) {
                        res[i].qr = await qrcode.toDataURL(req.headers.host + '/qr/' + res[i].uuid+'/visit')
                        result.push(res[i]);
                    }
                }
                callback(null, result);
            });
        }

        generateQRCode(function (err, products) {
            if (err)
                products = []
            res.render("qr_codes/qr.ejs", {
                messages,
                products,
                status,
                message
            })
        })
    },

    async show(req, res) {

        let uuid = req.params.uuid ? req.params.uuid : ''
        let messages = utility.getLang(); 
        connection.query(`SELECT * FROM qr_codes WHERE uuid = '${uuid}'`, async function (err, result, fields) {
            if (err || !result.length ){
                req.session.message = 'No record found!'; // resets session variable
                req.session.status = 'danger'; // resets session variable
                res.redirect('/qr');
            }
           
            result[0].qr = await qrcode.toDataURL(req.headers.host + '/qr/' + result[0].uuid+'/visit')
            let product = result[0]
            res.render("qr_codes/single.ejs", {
                messages,
                product
            })
        })
    },

    async visit(req, res) {

        let uuid = req.params.uuid ? req.params.uuid : ''
        let messages = utility.getLang();
        connection.query(`SELECT * FROM qr_codes WHERE uuid = '${uuid}'`, async function (err, result, fields) {
            if (err || !result.length ){
                req.session.message = 'No record found!'; // resets session variable
                req.session.status = 'danger'; // resets session variable
                res.redirect('/qr');
            }

            connection.query(`UPDATE qr_codes SET last_visited =  '${new Date().toGMTString()}'  WHERE status = '1' AND uuid = '${uuid}'`);

            result[0].qr = await qrcode.toDataURL(req.headers.host + '/qr/' + result[0].uuid)
            let product = result[0]
            res.render("qr_codes/single.ejs", {
                messages,
                product
            })
        })
    },

    create(req, res) {
        let count = req.body.qr_code_count ? req.body.qr_code_count : 0
        let product = {
            "uuid": uuidv4(),
            "created_by": "Auth User",
            "last_visited": null,
            "product_id": Math.floor(Math.random() * (10000000 - 1)) + 1,
            "status": 1,
            count: count
        }

        connection.query('INSERT INTO qr_codes SET ?', product, function (error, results, fields) {

            if (error) throw error;
            // Neat!
            //connection.release()
            req.session.status = 'success'; // resets session variable
            req.session.message = 'Record Created successfully!';
            res.redirect('/qr');
        });
    }
}