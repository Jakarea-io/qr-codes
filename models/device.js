const utility = require("./utility");
const session = require("express-session");

class device {
    constructor(action, data) {
        this.data = data;
        this.action = action;
    }
    output() {
        switch (this.action) {
            case "add":
                return this.add();
            default:
                return Promise.reject(
                    utility.Response("warning", "default", 0, "Action not found")
                );
        }
    }
    
    add() {
        let data = this.data;
        let platform = data.payload.platform;
        if (platform == undefined || platform == "") {
            return Promise.reject(
                utility.Response("error", "add", 1, "Platform is empty")
            );
        }
        let imei = data.payload.imei;
        if (imei == undefined || imei == "") {
            return Promise.reject(
                utility.Response("error", "add", 1, "imei is empty")
            );
        }
        let model = data.payload.model;
        if (model == undefined || model == "") {
            return Promise.reject(
                utility.Response("error", "add", 1, "Model is empty")
            );
        }
        let vendor = data.payload.vendor;
        if (vendor == undefined || vendor == "") {
            return Promise.reject(
                utility.Response("error", "add", 1, "Vendor is empty")
            );
        }

        let stmt = `INSERT INTO devices (platform, imei, model, vendor) VALUES (?, ?, ?, ?)`;
        let tbvalue = [];

        tbvalue = [
            platform,
            imei,
            model,
            vendor
        ];

        return utility.Database(stmt, tbvalue).then(function (reslt) {
            return Promise.resolve(
                utility.Response(
                    "success",
                    "add",
                    1,
                    "new device added!",
                    tbvalue
                )
            );
        });
    }
}
module.exports = device;