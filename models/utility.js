const con = require("./db_connection");
const jwt = require("jsonwebtoken");
const PathM = require("path");
const crypto = require("crypto");
const lang = require("./lang");
const ALOG = "AES-256-CBC";
const secret = "3s8VgUJCRAPkKrdLShTbPQh2yby9SPx6";
const iv = secret.substr(0, 16);
const SECRET_KEY = "DLAaZcKH59l1yHk954jyuiyWHwH7g";
const fs = require("fs");
const nodemailer = require("nodemailer");
const API_KEY ="";

class Utility {
  getLang() {
    return lang.en;
  }

  getUrl() {
    let APP_URL = "http://localhost:8000";
    if (process.env.NODE_ENV === "production") {
      APP_URL = "https://app.secupohl.com";
    }
    return APP_URL;
  }

  /**
   * @description This method returns response for all api calls
   * @param {*} type
   * @param {*} action
   * @param {*} status
   * @param {*} response
   * @param {*} data
   * @returns object
   */
  Response(type, action, status = "", response = "", data = {}) {
    return {
      type,
      action,
      status,
      response,
      data
    };
  }

  Database(query, data) {
    // console.log(data);
    return new Promise((resolve, reject) => {
      // return resolve(query);
      let t = con.query(query, data, (err, results) => {
        console.log(t.sql);
        if (err) {
          // console.log(t.sql);
          return reject(err);
        } else {
          return resolve(results);
        }
      });
    });
  }

  encrypt(data) {
    var encryptor = crypto.createCipheriv(ALOG, secret, iv);
    return encryptor.update(data, "utf8", "base64") + encryptor.final("base64");
  }

  decrypt(data) {
    var msg = "";
    var decryptor = crypto.createDecipheriv(ALOG, secret, iv);
    try {
      msg = decryptor.update(data, "base64", "utf8") + decryptor.final("utf8");
    } catch (ex) {
      console.log("Error On Decrypt: ", data);
    }
    return msg;
  }

  decryptStrong(data) {
    const crypto = require("crypto");
    const ALOG = "AES-256-CBC";
    const secret = "fYxbX5j2ejDBt59NAvdPGKf4YvZ6fFwa";
    const iv = secret.substr(0, 16);

    data = decodeURIComponent(data);
    let msg = "";
    let decrypt = crypto.createDecipheriv(ALOG, secret, iv);
    try {
      msg = decrypt.update(data, "base64", "utf8") + decrypt.final("utf8");
    } catch (ex) {
      console.log("Error On Decrypt: ", ex);
      return false;
    }
    return msg;
  }

  encryptStrong(data) {
    data = JSON.stringify(data);
    const crypto = require("crypto");
    const ALOG = "AES-256-CBC";
    const secret = "fYxbX5j2ejDBt59NAvdPGKf4YvZ6fFwa";
    const iv = secret.substr(0, 16);
    let encryptor = crypto.createCipheriv(ALOG, secret, iv);
    let enc =
      encryptor.update(data, "utf8", "base64") + encryptor.final("base64");

    return encodeURIComponent(enc);
  }

  generateJWTToken(userData) {
    return jwt.sign(userData, SECRET_KEY);
  }

  verifyToken(jwtToken) {
    return new Promise((resolve, reject) => {
      const authHeader = jwtToken;
      if (authHeader) {
        const token = authHeader.split(" ")[1];
        jwt.verify(token, SECRET_KEY, (err, user) => {
          if (err) {
            req.session.destroy();
            reject(utility.Response("error", "user", 0, "Invalid token"));
          } else if (user && user[0]["email"] && user[0]["id"]) {
            resolve(true);
          }
        });
      } else {
        req.session.destroy();
        reject(false);
      }
    });
  }

  validateCharacter(data) {
    var reg = /[^a-zA-Z ]/g;
    if (data.match(reg)) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(data) {
    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (data.match(reg)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @description validate email
   * @param data
   * @returns {boolean}
   */
  validatePhone(data) {
    var reg = /^((?:\+\d+)?\s*(?:\(\d+\)\s*(?:[\/–-]\s*)?)?\d+(?:\s*(?:[\s\/–-]\s*)?\d+)*)$/;
    if (data.match(reg)) {
      return true;
    } else {
      return false;
    }
  }

  validatePwd(str) {
    if (
      str.match(/[a-z]/g) &&
      str.match(/[A-Z]/g) &&
      str.match(/[0-9]/g) &&
      str.match(/[^a-zA-Z\d]/g)
    ) {
      return true;
    } else {
      return false;
    }
  }

  validatePassword(pwd) {
    if (pwd) {
      if (pwd.length < 8) {
        return messages.password_length_issue;
      }
      if (pwd.length > 15) {
        return messages.password_length_issue;
      } else if (!this.validatePwd(pwd)) {
        return messages.passwrord_strong_policy;
      } else return true;
    } else {
      return messages.password_empty;
    }
  }

  uploadImagefile(dataString, path = false) {
    return new Promise((resolve, reject) => {
      try {
        if (dataString) {
          let fileName = Date.now();
          let p = path
            ? path
            : PathM.resolve(__dirname, "../upload/" + fileName + ".png");
          let base64Data = dataString.replace(
            /^data:([A-Za-z-+/]+);base64,/,
            ""
          );
          fs.writeFileSync(p, base64Data, "base64");
          return resolve(fileName + ".png");
        } else {
          return reject(false);
        }
      } catch (e) {
        return reject(e.stack);
      }
    });
  }

  sendMail(email, subject, html) {
    return new Promise((resolve, reject) => {
      let transporter = nodemailer.createTransport({
        host: "smtp.sendgrid.net",
        port: 587,
        secure: false, // upgrade later with STARTTLS
        auth: {
          user: "apikey",
          pass: API_KEY
        }
      });

      let mailOptions = {
        from: "app@secupohl.com",
        to: email,
        subject: subject,
        html: html
      };

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
          return reject(error);
        } else {
          console.log("Email sent: " + info.response);
          return resolve(info.response);
        }
      });
    });
  }
}

module.exports = new Utility();
