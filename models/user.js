const utility = require("./utility");
const session = require("express-session");
const APP_URL = utility.getUrl();


class User {
    constructor(action, data) {
        this.data = data;
        this.action = action;
    }

    output() {
        switch (this.action) {
            case "register":
                return this.register();
            case "check":
                return Promise.resolve("It's working");
            case "login":
                return this.login();
            default:
                return Promise.reject(
                    utility.Response("user", "warning", "Action not found")
                );
        }
    }

    register() {
        let data = this.data;
        let name_title = data.payload.name_title;
        let first_name = data.payload.first_name;
        let sur_name = data.payload.sur_name;
        let phone = data.payload.phone;
        let email = data.payload.email;
        let password = data.payload.password;
        let confirm_password = data.payload.confirm_password;
        let first_name_valid = utility.validateCharacter(first_name);
        let sur_name_valid = utility.validateCharacter(sur_name);
        let phone_valid = utility.validatePhone(phone);
        let email_valid = utility.validateEmail(email);
        let pwd_valid = true; //utility.validatePwd(password);
        let confirm_pwd_valid = true; //utility.validatePwd(confirm_password);
        let pwd_encrypt = utility.encrypt(password);

        // console.log(password confirm_password);
        if (name_title == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter Title of Name")
            );
        } else if (first_name == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter First Name")
            );
        } else if (first_name != "" && first_name_valid == false) {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter valid First Name")
            );
        } else if (sur_name == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the Sur Name")
            );
        } else if (sur_name != "" && sur_name_valid == false) {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter valid Sur Name")
            );
        } else if (phone == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the Phone")
            );
        } else if (phone != "" && phone_valid == false) {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter valid Phone")
            );
        } else if (email == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the Email Address")
            );
        } else if (email != "" && email_valid == false) {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter valid Email Address")
            );
        } else if (password == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the password")
            );
        } else if (password !== confirm_password) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password and Confirmation Password not match"
                )
            );
        } else if (password != "" && password < 8) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password must be at least 8 characters "
                )
            );
        } else if (password != "" && password > 15) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password cannot be greater than 15 characters "
                )
            );
        } else if (password != "" && pwd_valid == false) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password must be at least 1 uppercase,lowercase and digit character."
                )
            );
        } else if (confirm_password == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the Confirm Password")
            );
        } else if (confirm_password != "" && confirm_password < 8) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Confirm Password must be at least 8 characters "
                )
            );
        } else if (confirm_password != "" && confirm_password > 15) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Confirm Password cannot be greater than 15 characters "
                )
            );
        } else if (confirm_password != "" && confirm_pwd_valid == false) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Confirm Password must be at least 1 uppercase,lowercase and digit character."
                )
            );
        } else {
            return utility
                .Database(
                    "SELECT count(*) as count  FROM secupohl_login WHERE email=?",
                    [email]
                )
                .then(function (result) {
                    let count = result[0].count;

                    if (count == 0) {
                        let stmt = `INSERT INTO secupohl_login(title, first_name, sur_name, phone_no, email, password, status)
                                    VALUES (?, ?, ?, ?, ?, ?, ?)`;
                        let tbvalue = [
                            name_title,
                            first_name,
                            sur_name,
                            phone,
                            email,
                            pwd_encrypt,
                            1,
                        ];
                        return utility.Database(stmt, tbvalue).then(function (reslt) {
                            return Promise.resolve(
                                utility.Response(
                                    "success",
                                    "user",
                                    1,
                                    "Registered Successfully"
                                )
                            );
                        });
                    } else {
                        return utility
                            .Database(
                                "SELECT count(*) as count  FROM secupohl_login WHERE email=? and new_user=?",
                                [email, 0]
                            )
                            .then(function (result) {
                                let count = result[0].count;
                                if (count == 0) {
                                    let stmt = `UPDATE secupohl_login
                                                SET title=?,
                                                    first_name=?,
                                                    sur_name=?,
                                                    phone_no=?,
                                                    password=?,
                                                    status=?,
                                                    new_user=?
                                                where email = ?`;
                                    let tbvalue = [
                                        name_title,
                                        first_name,
                                        sur_name,
                                        phone,
                                        pwd_encrypt,
                                        1,
                                        0,
                                        email,
                                    ];
                                    return utility.Database(stmt, tbvalue).then(function (reslt) {
                                        return Promise.resolve(
                                            utility.Response(
                                                "success",
                                                "user",
                                                1,
                                                "Registered Successfully"
                                            )
                                        );
                                    });
                                } else {
                                    return Promise.resolve(
                                        utility.Response(
                                            "error",
                                            "user",
                                            0,
                                            "Email Address already exists"
                                        )
                                    );
                                }
                            });
                    }
                });
        }
    }

    login() {
        let data = this.data;
        let email = data.payload.email;
        let password = data.payload.password;
        let email_valid = utility.validateEmail(email);
        let pwd_valid = utility.validatePwd(password);
        let pwd_cache = utility.encrypt(password);
        //let strongpwd=utility.validatePwd(password);

        if (email == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Email is empty")
            );
        } else if (email != "" && email_valid == false) {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter valid Email Address")
            );
        } else if (password == "") {
            return Promise.reject(
                utility.Response("error", "user", 0, "Enter the password")
            );
        } else if (password != "" && password < 8) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password must be at least 8 characters "
                )
            );
        } else if (password != "" && password > 15) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password cannot be greater than 15 characters "
                )
            );
        } else if (password != "" && pwd_valid == false) {
            return Promise.reject(
                utility.Response(
                    "error",
                    "user",
                    0,
                    "Password must be at least 1 uppercase,lowercase and digit character."
                )
            );
        } else {
            if (session && session.userid) {
                return Promise.resolve({
                    status: 1,
                    data: session.token,
                    token: session.token,
                    userid: session.user_id,
                });
            } else {
                return utility
                    .Database(
                        "SELECT id, first_name,email,company_id FROM secupohl_login  WHERE status=1 AND email=? AND password=?",
                        [email, pwd_cache]
                    )
                    .then(function (result) {
                        if (result && result.length > 0) {
                            let user = JSON.stringify(result);
                            let token = utility.generateJWTToken(user);
                            let d = result[0];
                            session.userid = d.id;
                            session.first_name = d.first_name;
                            session.company_id = d.company_id;
                            session.token = token;
                            return utility
                                .Database(
                                    "SELECT count(*) as count  FROM company_detail WHERE user_id =?",
                                    [result[0].id]
                                )
                                .then((res) => {
                                    var first_view = "block_in_first_reg";
                                    if (res && res[0] && res[0]["count"] > 0) {
                                        first_view = "bypass_first_reg";
                                    }
                                    return Promise.resolve(
                                        utility.Response("success", "user", 1, {
                                            msg: "successfully login",
                                            token: token,
                                            user: result[0].id,
                                            first_view: first_view,
                                        })
                                    );
                                });
                        } else {
                            return Promise.reject(
                                utility.Response("error", "user", 0, "Invalid User")
                            );
                        }
                    });
            }
        }
    }
}

module.exports = User;
