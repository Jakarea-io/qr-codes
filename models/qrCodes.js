const utility = require("./utility");
const session = require("express-session");
const { v4: uuidv4 } = require('uuid');
const qrcode = require('qrcode');
const APP_URL = utility.getUrl();

class QRCodes {
    constructor(action, data) {
        this.data = data;
        this.action = action;
    }
    output() {
        switch (this.action) {
            case "create":
                return this.create();
            case "visit":
                return this.visit();
            case "delete":
                return this.delete();
            default:
                return Promise.reject(
                    utility.Response("qrcodes", "warning", "Action not found")
                );
        }
    }
    async create() {
        let data = this.data;
        let count = data.payload.count;
        let product_id = data.payload.product_id;

        if (count == undefined || count == "") {
            return Promise.reject(
                utility.Response("error", "qrcodes", 0, "Count is empty")
            );
        } else if (product_id == undefined || product_id == "") {
            return Promise.reject(
                utility.Response("error", "qrcodes", 0, "Product ID is empty")
            );
        } else {
            let stmt = `INSERT INTO qr_codes(uuid, created_by, last_visited, product_id, status) VALUES (?, ?, ?, ?, ?)`;
            let tbvalue = [];
            let uuid = uuidv4()
            let qrCode = await qrcode.toDataURL(uuid)
            tbvalue = [
                uuid,
                "Auth User",
                null,
                product_id,
                1,
            ];

            // for(var i = 0; i<count; i++){
            //     let qr = [
            //         uuidv4(),
            //         "Auth User",
            //         null,
            //         i,
            //         1,
            //     ];
            //     tbvalue.push(qr)
            // }

            return utility.Database(stmt, tbvalue).then(function (reslt) {

                let product = {
                    "uuid": uuid,
                    "product_id": product_id,
                    "last_visited": '',
                    "created_by": "Auth User",
                    "status": 'Active',
                    "qr": qrCode
                }

                return Promise.resolve(
                    utility.Response(
                        "success",
                        "qrcodes",
                        1,
                        "QR code inserted Successfully",
                        product
                    )
                );
            });
        }
    }
    visit() {
        let data = this.data;
        let uuid = data.query.uuid;
        if (uuid == undefined || uuid == "") {
            return Promise.reject(
                utility.Response("error", "qrcodes", 0, "uuid is empty")
            );
        }
        let stmt = `UPDATE qr_codes 
                        SET last_visited=?
                        WHERE status = 1
                        AND uuid = ?`;
        let tbvalue = [new Date().toGMTString(), uuid];
        return utility.Database(stmt, tbvalue).then(function (reslt) {
            return Promise.resolve(
                utility.Response(
                    "success",
                    "qrcodes",
                    1,
                    1
                )
            );
        });
    }

    delete() {
        let data = this.data;
        let uuid = data.query.uuid;
        if (uuid == undefined || uuid == "") {
            return Promise.reject(
                utility.Response("error", "qrcodes", 0, "uuid is empty")
            );
        }
        let stmt = `DELETE FROM qr_codes 
                        WHERE uuid = ?`;
        let tbvalue = [uuid];
        return utility.Database(stmt, tbvalue).then(function (result) {
            return Promise.resolve(
                utility.Response(
                    "success",
                    "qrcodes",
                    result.affectedRows,
                    "QR code deleted"
                )
            );
        });
    }
}
module.exports = QRCodes;