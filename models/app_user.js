const utility = require("./utility");
const session = require("express-session");
const con = require("./db_connection");

class app_user {
    constructor(action, data) {
        this.data = data;
        this.action = action;
    }
    output() {
        switch (this.action) {
            case "signup":
                return this.signup();
            case "verify-otp":
                return this.verifyOtp();
            case "create-profile":
                return this.createProfile();
            case "edit-profile":
                return this.editProfile();
            default:
                return Promise.reject(
                    utility.Response("warning", "app-user", 0, "Action not found")
                );
        }
    }
    signup() {
        let data = this.data;
        let phone = data.payload.phone;
        if (phone == undefined || phone == "") {
            return Promise.reject(
                utility.Response("error", "signup", 1, "Phone is empty")
            );
        }

        return utility
            .Database(
                "SELECT count(*) as count  FROM app_users WHERE phone =?",
                [phone]
            )
            .then((res) => {
                if (res && res[0] && res[0]["count"] > 0) {
                    return Promise.resolve(
                        utility.Response("warning", "signup", 1, "Phone already exist!", { phone })
                    );
                } else {
                    let otp = Math.floor(1000 + Math.random() * 9000);
                    let device_id = Math.random().toString(36).substring(2);

                    let stmt = `INSERT INTO otps (device_id, otp) VALUES (?, ?)`;
                    let tbvalue = [];

                    tbvalue = [
                        device_id,
                        otp
                    ];
                    var query = con.query('INSERT INTO app_users SET ?', { phone }, function (error, results, fields) {
                        if (error) throw error;
                        return Promise.resolve(
                            utility.Response("warning", "signup", 1, "Phone already exist!")
                        );
                    });
                    return utility.Database(stmt, tbvalue).then(function (reslt) {
                        data = {
                            otp,
                            device_id,
                            phone
                        }
                        return Promise.resolve(
                            utility.Response(
                                "success",
                                "signup",
                                1,
                                "OTP sent",
                                data
                            )
                        );
                    });
                }
            });
    }

    verifyOtp() {
        let data = this.data;
        let otp = data.payload.otp;
        let device_id = data.payload.device_id;

        if (otp == undefined || otp == "") {
            return Promise.reject(
                utility.Response("error", "verify-otp", 1, "otp is empty")
            );
        }

        if (device_id == undefined || device_id == "") {
            return Promise.reject(
                utility.Response("error", "verify-otp", 1, "device_id is empty")
            );
        }

        return utility
            .Database(
                "SELECT *  FROM otps WHERE otp =? and device_id = ? ",
                [otp, device_id]
            )
            .then((res) => {
                if (res.length > 0) {
                    let diffInSecond = (new Date() - res[0].created_on) / 1000
                    console.log('res 2')
                    let stmt = `UPDATE otps SET otp=?,
                                            verified_on=?
                                            where otp = ?
                                            and device_id = ?`;
                    let tbvalue = [
                        null,
                        new Date(),
                        otp,
                        device_id,
                    ];

                    return utility.Database(stmt, tbvalue).then(function (reslt) {
                        data = {
                            otp,
                            device_id
                        }

                        return Promise.resolve(
                            utility.Response(
                                "warning",
                                "verify-otp",
                                1,
                                "OTP verified!",
                                data
                            )
                        );
                    });
                } else {

                    return Promise.resolve(
                        utility.Response(
                            "warning",
                            "verify-otp",
                            1,
                            "OTP does not matches or expired!",
                            {
                                otp,
                                device_id
                            }
                        )
                    );
                }
            });
    }

    createProfile() {
        let data = this.data;
        console.log({'data' : this.data})


        let first_name = data.payload.first_name;
        if (first_name == undefined || first_name == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "First name is empty")
            );
        }
        let last_name = data.payload.last_name;
        if (last_name == undefined || last_name == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Last name is empty")
            );
        }
        let profile_pic = data.payload.profile_pic;
        if (profile_pic == undefined || profile_pic == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Profile picture is empty")
            );
        }

        let address = data.payload.address;
        if (address == undefined || address == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Address is empty")
            );
        }
        
        let stmt = `INSERT INTO profiles (first_name, last_name, profile_pic, address) VALUES (?, ?, ?, ?)`;
        let tbvalue = [];
        tbvalue = [
            first_name,
            last_name,
            profile_pic,
            address
        ];

        return utility.Database(stmt, tbvalue).then(function (reslt) {
            return Promise.resolve(
                utility.Response(
                    "success",
                    "create-profile",
                    1,
                    "Profile updated!",
                    tbvalue
                )
            );
        });
    }

    editProfile() {
        let data = this.data;
        let id = data.payload.id;
        if (id == undefined || id == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "ID is empty")
            );
        }
        let first_name = data.payload.first_name;
        if (first_name == undefined || first_name == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "First name is empty")
            );
        }
        let last_name = data.payload.last_name;
        if (last_name == undefined || last_name == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Last name is empty")
            );
        }
        let profile_pic = data.payload.profile_pic;
        if (profile_pic == undefined || profile_pic == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Profile picture is empty")
            );
        }

        let address = data.payload.address;
        if (address == undefined || address == "") {
            return Promise.reject(
                utility.Response("error", "create-profile", 1, "Address is empty")
            );
        }

        let stmt = `UPDATE profiles SET first_name=?,
                        last_name=?,
                        profile_pic=?,
                        address=?
                        where id = ?`;
        let tbvalue = [];

        tbvalue = [
            first_name,
            last_name,
            profile_pic,
            address,
            id
        ];

        return utility.Database(stmt, tbvalue).then(function (reslt) {
            return Promise.resolve(
                utility.Response(
                    "success",
                    "add",
                    1,
                    "Profile created!",
                    tbvalue
                )
            );
        });
    }
}
module.exports = app_user;